const express = require("express");
const router = express.Router();
const knex = require("knex");
const config = require("../knexfile").development;
const t = require('tcomb-validation');
var validations =  require('./validations');
const auth=require('./auth')();
const passport = require('passport');
let jwt = require('jwt-simple');
let cfg = require('./jwt_config.js');



const reqBodyValidation=(req, res, next) => {
    let result =t.validate(req.body,validations.loginData);
      if (result.isValid()) next();
    else res.status(400).json(result.errors);
   };

router.post("/",reqBodyValidation, (req, res) => {
    knex(config)
        .returning("*")
        .insert(req.body)
        .into("loginData")
        .then(function(data) {
            res.status(200).json({message:" user added sucessfully"})
        
        })
        .catch(e=> res.status(400).json({message:"user not added"}))
    });
    

router.post('/token', async(req, res) => {
const x = await knex(config)
   .where({
    Email_id: req.body.Email_id,
    password: req.body.password
     })
     .from('loginData')
   console.log(x)

   if(x[0]){
       res.json({
        token: jwt.encode({Email_id:req.body.Email_id}, cfg.jwtSecret),
        user:x
       })
   }
   else{
       res.status(404).json("User not Existed")
   }
}) 
module.exports = router;