exports.up = function(knex, Promise) {
    return Promise.all([
        knex.schema.createTable("loginData", function(table) {
            table.increments("id").primary();
            table.string("firstname");
            table.string("lastname");
            table.string("Email_id");
            table.string("password");
            table.boolean("isActive");
           
        })
    ]);
};

exports.down = function(knex, Promise) {
    return Promise.all([knex.schema.dropTable("loginData")]);
};